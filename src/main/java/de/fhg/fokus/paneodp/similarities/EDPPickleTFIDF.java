package de.fhg.fokus.paneodp.similarities;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by Nils Rethmeier on 15.10.15.
 *
 */
public class EDPPickleTFIDF implements Serializable {
  /** FIXME if you can! BEGIN RANT: basically the same as {@link de.fhg.fokus.paneodp.similarities.EDPPickleTF()} but with doubles instead of integers
   * a generics Numbers version causes code rewrites somewhere else, inheritance is only slightly less code redundant and
   * and other generics dont quite work. Maybe I missed sth. C++ templates are still better though.
   * (╯°□°）╯︵ ┻━┻
   * Members that stores TFIDF info
   * Class very similar to   /* Members that store info necessary for TFIDF
   * Class very similar to {@link de.fhg.fokus.paneodp.similarities.EdpDocument()}
  **/
    public String docID = "";
    public HashMap<String, Double> title = new HashMap<String, Double>();
    public HashMap<String, Double> description = new HashMap<String, Double>();
    public HashSet<String> tags = new HashSet<String>();
    public HashSet<String> categories = new HashSet<String>();

//    // C'TOR
//    public EDPPickleTFIDF(String docID, HashMap<String, Double> title, HashMap<String, Double> description, HashSet<String> tags, HashSet<String> categories) {
//        /**
//         * Class to represent a single document, struct style.
//         */
//        if (docID != "" && !title.isEmpty() && !description.isEmpty() && !tags.isEmpty() && !categories.isEmpty()) {
//            this.docID = docID;
//            this.title = title;
//            this.description = description;
//            this.tags = tags;
//            this.categories = new HashSet<String>();
//            // remove uri start stuff
//            for (String url : categories) {
//                this.categories.add(getEnding(url));
//            }
//        } else {
//            throw new NullPointerException("one of the values is empty but should not be");
//        }
//    }

    // C'TOR for dirty data
    public EDPPickleTFIDF(String docID, HashMap<String, Double> title, HashMap<String, Double> description, HashSet<String> tags, HashSet<String> categories) {
        /**
         * Class to represent a single document, struct style.
         */
        // TODO NO LOGGER HERE! This is a lot of unnecessary overhead during de-serialization.
        Boolean DEBUG = false; // turn on for seeing empty data set with missing field values
        this.docID = docID;
        this.title = title;
        this.description = description;
        this.tags = tags;
        this.categories = new HashSet<String>();
        // remove uri start stuff
        for (String url : categories) {
            this.categories.add(getEnding(url));
        }
        if (DEBUG && docID != "" && !title.isEmpty() && !description.isEmpty() && !tags.isEmpty() && !categories.isEmpty()) {
            // DEBUG in front for early fail
            // Intentionally, No logger!
            System.out.println("Warning: document "+docID + " has empty value fields, i.e. is incomplete");
        }
    }

    public void print() {
        System.out.println("ID:" + this.docID + "\n"
                + "title:" + this.title + "\n"
                + "descr:" + this.description + "\n"
                + "tags:" + this.tags + "\n"
                + "categories:" + this.categories + "\n");
    }

    public String removeExtraSpaces(String org) {
        return org.replaceAll("\\r", "").replaceAll("\t", "").replaceAll("\\s+", " ").trim();
    }

    public String getEnding(String url) {
        return url.substring(url.lastIndexOf('/') + 1, url.length());
    }

    public String toString(){
        return "ID:" + this.docID + "\n"
                + "title:" + this.title + "\n"
                + "descr:" + this.description + "\n"
                + "tags:" + this.tags + "\n"
                + "categories:" + this.categories + "\n";
    }
}
