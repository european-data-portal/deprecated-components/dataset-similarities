package de.fhg.fokus.paneodp.similarities;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by nils rethmeier on 24.08.15.
 */
public class ObjectListIterator {
    /**
     * Generator pattern: allows to itterate/stream over serialized objects in a file (list of single objects) as if it
     * were a list of objects in memory, Point being: Its out of core. Java version of python generators.
     * <p>
     * Check http://www.codeproject.com/Articles/793374/Generators-with-Java for an explained exmaple. OR
     * https://en.wikipedia.org/wiki/Generator_%28computer_programming%29#Java
     */
//    private ObjectInputStream ois;  // stream on list of objects, read
//    private FileOutputStream fos; // -"-                        write
//
//    public ObjectListIterator(String fileName) {
//        this.ois = new ObjectInputStream(new FileInputStream(fileName));
//    }

    public static final String FILENAME = "cool_file.tmp";

//    public EDPDoc next(String filename) throws IOException, ClassNotFoundException {
//        // FIXME do this http://mechanical-sympathy.blogspot.jp/2012/07/native-cc-like-performance-for-java.html OR
//        // this http://stackoverflow.com/questions/7826834/de-serializing-objects-from-a-file-in-java
//        List<Object> results = new ArrayList<Object>();
//
//        FileInputStream fis = null;
//        try {
//            fis = new FileInputStream(FILENAME);
//            while (true) {
//                ObjectInputStream ois = new ObjectInputStream(fis);
//                results.add(ois.readObject());
//            }
//        } catch (EOFException ignored) {
//            // as expected
//        } finally {
//            if (fis != null)
//                fis.close();
//        }
//
//        return fstream.next();
//    }
//
//    private int i = 1;
//
//    public int next()
//    {
//        int thisOne = i++;
//        return thisOne * thisOne;
//    }
//
//
//    public void storeIterator(String fileName) throws IOException, ClassNotFoundException {
//        /**
//         * http://stackoverflow.com/questions/7826834/de-serializing-objects-from-a-file-in-java
//         */
//        // FIXME do this
//        FileOutputStream fos = null;
//        try {
//            fos = new FileOutputStream(FILENAME);
//            for (String s : test.split("\\s+")) {
//                ObjectOutputStream oos = new ObjectOutputStream(fos);
//                oos.writeObject(s);
//            }
//        } finally {
//            if (fos != null)
//                fos.close();
//        }
//    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        String test = "This will work if the objects were written with a single ObjectOutputStream. \n" +
                "If several ObjectOutputStreams were used to write to the same file in succession,\n" +
                "it will not. – Tom Anderson 4 mins ago";

        FileOutputStream fos = null;
        int i=0;
        try {
            fos = new FileOutputStream(FILENAME);
            for (String sent : test.split("\n")) {
                String[] words = sent.split("\\s+");
                // Put title
                HashMap<String, Integer> title = new HashMap<String, Integer>();
                for (String word : sent.substring(0, 10).split("\\s+")) {
                    title.put(word, 1);
                }
                // Put descrition
                HashMap<String, Integer> desc = new HashMap<String, Integer>();
                for (String word : sent.split("\\s+")) {
                    desc.put(word, 1);
                }
                // tags
                HashSet<String> tags = new HashSet<String>();
                for (String word : sent.substring(0,10).split("\\s+")) {
                    tags.add(word.substring(0, 1));
                }
                // categoy
                // tags
                HashSet<String> categ = new HashSet<String>();
                for (String word : sent.substring(0,10).split("\\s+")) {
                    categ.add(word.substring(1, 2));
                }
                EDPPickleTF d = new EDPPickleTF(words[0], title, desc, tags, categ);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(d);

            }
        } finally {
            if (fos != null)
                fos.close();
        }

        List<EDPPickleTF> results = new ArrayList<EDPPickleTF>();

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(FILENAME);
            while (true) {
                ObjectInputStream ois = new ObjectInputStream(fis);
                EDPPickleTF pkl = (EDPPickleTF)ois.readObject();
                results.add(pkl);
            }
        } catch (EOFException ignored) {
            // as expected
        } finally {
            if (fis != null)
                fis.close();
        }
        for (EDPPickleTF p : results) {
            p.print();
        }
    }

}
