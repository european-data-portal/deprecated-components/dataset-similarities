package de.fhg.fokus.paneodp.similarities;

/**
 * Created by nils rethmeier on 21.10.15.
 */
public class MatchingDataSet {
    /**
     * Simple class to store matching distributions for a dataset /in a catalog.
     */
    public String originDistributionKey;
    public String matchingDistributionKey;
    public String matchingDatasetKey;

    public MatchingDataSet( String originDistributionKey,
                            String matchingDistributionKey,
                            String matchingDatasetKey) {
        this.originDistributionKey   = originDistributionKey;
        this.matchingDistributionKey = matchingDistributionKey;
        this.matchingDatasetKey      = matchingDatasetKey;
    }
}
