package de.fhg.fokus.paneodp.similarities;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by nils rethmeier on 20.09.15.
 */
public class EdpTermFrequencyInfo {
    /**
     * Created by Nils Rethmeier on 28.08.15.
     * Abstracts the information content (TFIDF info) of a document in the EDP document collection.
     */
    public class EdpDocument implements java.io.Serializable {
        public String docID;
        public HashMap<String, Integer> title;
        public HashMap<String, Integer> descriptionTF;
        public HashSet<String> tags;
        public HashSet<String> categories;

        public EdpDocument(String docID, HashMap<String, Integer> title, HashMap<String, Integer> description, HashSet<String> tags, HashSet<String> categories,
                           String URLtail) {
            /**
             * Class to represent a single document, struct style.
             */
            if (docID != "" && !title.isEmpty() && !description.isEmpty() && !tags.isEmpty() && !categories.isEmpty()) {
                this.docID = docID;
                this.title = title;
                this.descriptionTF = description;
                this.tags = tags;
                this.categories = categories;
            } else {
                throw new NullPointerException("one of the values is empty but should not be");
            }
        }

        public void print() {
            System.out.println(this.docID + "\n"
                    + this.title + "\n"
                    + this.descriptionTF + "\n"
                    + this.tags + "\n"
                    + this.categories + "\n");
        }
    }
}
